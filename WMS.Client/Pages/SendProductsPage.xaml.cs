﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WMS.Client.Operations;

namespace WMS.Client.Pages
{
    /// <summary>
    /// Interaction logic for SendProductsPage.xaml
    /// </summary>
    public partial class SendProductsPage : Page
    {
        public SendProductsPage()
        {
            InitializeComponent();
        }

        private void btnSendProducts_Click(object sender, RoutedEventArgs e)
        {
            var quantity = quantityControl.Value;

            if (quantity.HasValue && quantity > 0 && quantity <= int.MaxValue)
            {
                var api = new ApiCalls();
                var addQuantity = api.AddProductQuantity(quantity.Value);

                if (addQuantity)
                {
                    MessageBox.Show("New Product Quantity sent successfully.");
                    quantityControl.Value = null;
                }
                else
                {
                    MessageBox.Show("Error sending new Product Quantity.");
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid Product Quantity!");                   
            }
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            Globals.AuthenticatedUser = null;
            NavigationService.Navigate(new LoginPage());
        }
    }
}
