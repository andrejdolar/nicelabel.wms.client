﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WMS.Client.Operations;

namespace WMS.Client.Pages
{
    /// <summary>
    /// Interaction logic for RegistrationPage.xaml
    /// </summary>
    public partial class RegistrationPage : Page
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnReg_Click(object sender, RoutedEventArgs e)
        {
            var username = tbxUsername.Text;
            var password = pbxPassword.Password;
            var repeatPassword = pbxPasswordRepeat.Password;

            if (!repeatPassword.Equals(password,StringComparison.Ordinal))
            {
                MessageBox.Show("Passwords do not match!");
                return;
            }

            var api = new ApiCalls();

            var user = api.RegisterUser(username, password);

            if (user == null)
            {
                MessageBox.Show("Error creating user, check if username already exists.");
                return;
            }

            Globals.AuthenticatedUser = user;
            MessageBox.Show("Registration successful.");

            NavigationService.Navigate(new SendProductsPage());           
        }
    }
}
