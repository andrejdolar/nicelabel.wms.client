﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Client.Models
{
    class WMSUser
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string AccessToken { get; set; }

        public WMSProduct UserProduct { get; set; }
    }
}
