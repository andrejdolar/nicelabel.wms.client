﻿using WMS.Client.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace WMS.Client.Operations
{
    class ApiCalls
    {
        public WMSUser AuthenticateUser(string username, string password)
        {
            // TODO Use HTTPS - SSL for encryption
            //var encryptedPassword = EncryptOperations.EncryptData(Globals.EncryptCertificate, password);

            string endpoint = $"{ConfigurationManager.AppSettings["WmsServiceAddress"]}/Account/Login";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                UserName = username,
                Password = password
            });

            using (var wc = new WebClient())
            {
                wc.Headers["Content-Type"] = "application/json";
                try
                {
                    string response = wc.UploadString(endpoint, method, json);
                    return new WMSUser() { AccessToken = response };
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public WMSUser RegisterUser(string username, string password)
        {
            string endpoint = $"{ConfigurationManager.AppSettings["WmsServiceAddress"]}/Account/Register";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                UserName = username,
                Password = password
            });

            using (var wc = new WebClient())
            {
                wc.Headers["Content-Type"] = "application/json";
                try
                {
                    string response = wc.UploadString(endpoint, method, json);
                    return new WMSUser() { AccessToken = response };
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool AddProductQuantity(int productQuantity)
        {
            string endpoint = $"{ConfigurationManager.AppSettings["WmsServiceAddress"]}/Product/AddProductQuantity";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                Quantity = productQuantity
            });

            using (var wc = new WebClient())
            {
                wc.Headers["Content-Type"] = "application/json";
                wc.Headers["Authorization"] = $"Bearer {Globals.AuthenticatedUser.AccessToken}";
                try
                {
                    string response = wc.UploadString(endpoint, method, json);
                    return JsonConvert.DeserializeObject<bool>(response);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
