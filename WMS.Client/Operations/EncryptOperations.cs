﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Client.Operations
{
    class EncryptOperations
    {
        public static string EncryptData(X509Certificate2 cert, string data)
        {
            var bytesToEncrypt = ASCIIEncoding.ASCII.GetBytes(data);

            using (var rsa = cert.GetRSAPublicKey())
            {
                var encryptedBytes = rsa.Encrypt(bytesToEncrypt, RSAEncryptionPadding.OaepSHA1);
                return Convert.ToBase64String(encryptedBytes);
            }
        }
    }
}
