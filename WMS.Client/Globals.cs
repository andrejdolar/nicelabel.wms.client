﻿using WMS.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Client
{
    class Globals
    {
        public static WMSUser AuthenticatedUser { get; set; }
        public static X509Certificate2 EncryptCertificate
        {
            get { return new X509Certificate2(Properties.Resources.WMS_Certificate); }
        }
    }
}
